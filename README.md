<!--
**danema21/danema21** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

<h3 align="center">Hi, I'm Damian, a full-stack developer with expertise in Java, Spring Boot, Angular, and ReactJS. I specialize in implementing microservices following best practices and delivering efficient solutions. I also have a strong ability to translate design mockups into seamless UIs in Angular or ReactJS, ensuring a smooth user experience without compromising on quality.</h3>

  - 👨‍💻 Here are some links to my top repositories to showcase my work experience:
    
    ### Java developer two years+ experience
    - I'm working on a project consisting of a SpringBoot microservice and an Angular client. The purpose of this project is to serve the client with video streaming services using kafka. I learned from my previous projects and implemented test automation in this project too with the gitlabCI/CD pipelines.
      * link to SpringBoot Api repository: https://gitlab.com/uy.com.personal/video_streaming_services
      * link to Angular Client repository: https://gitlab.com/uy.com.personal/video_streaming_services_ui
      * the ui is a work in progress:
      * ![video_streaming_services_ui.gif](./video_streaming_services_ui.gif)
      * I deployed the backend and the frontend on render.com, you can visit this link: https://video-streaming-services-ui.onrender.com


    - I implemented a project where I was setting up test automation using gitlabCI/CD pipelines, I implemented demo unit tests and integration tests making use of mongodb and kafka.
      * link to repository: https://gitlab.com/uy.com.personal/pipeline_tests


    - I'm continuously working in a project where I implement everything new I learn as a Java developer working in Geocom. The architecture of the project is as follows: 
    
    * ![arquitectura_proyecto.png](./arquitectura_proyecto.png)

    - The user interface in Angular is consuming the audit-service api through the gateway to get and delete event and error audits, the structure is divided into multiple components to achieve good scalability:
    * ![interfaz_del_administrador_de_auditorias.png](./interfaz_del_administrador_de_auditorias.png)

    - This project has Unit Tests with Mockito and JUnit, Cucumber Integration Tests and generated test reports, Kafka producer and consumer, multithreading, batch processing, api-gateway, angular frontend secured with keycloak and docker-compose file for easily setting up the environment to run the whole project.
      * 💬 If you are interested in knowing how I aproached the implementation of this whole project then let's schedule an interview and I'll tell you more.
    
    ### UTEC academic frontend experience
    - I made a ReactJS app that consumes the free PokeApi service, I used axios to make the api calls, react-bootstrap for the designs and react-router for SPA navigation.
      - ![interfaz_poke-api.png](./interfaz_poke-api.png)
    
	    * link to repository: https://github.com/danema21/PokeAPI-front
	    * link to deployed app: https://danema21.github.io/PokeAPI-front
      

<h3 align="left">My social media:</h3>
<p align="left">
<a href="https://www.linkedin.com/in/damian-dalto-0b0a35227/" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="damian dalto" height="30" width="40" /></a>
</p>

<h3 align="left">Languages and tools:</h3>
<p align="left"> 
  <a href="https://getbootstrap.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="40" height="40"/> </a>
  <a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a>
  <a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> 
  <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a>
  <a href="https://www.java.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="40" height="40"/> </a>
  <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a>
  <a href="https://www.linux.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a>
  <a href="https://www.postgresql.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-original-wordmark.svg" alt="postgresql" width="40" height="40"/> </a>
  <a href="https://reactjs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/> </a> 
  <a href="https://angular.dev/" target="_blank" rel="noreferrer"> <img src="https://cdn.iconscout.com/icon/free/png-256/free-angular-2038881-1720094.png?f=webp&w=128" alt="angular" width="40" height="40"/> </a>
  <a href="https://spring.io/projects/spring-boot" target="_blank" rel="noreferrer"> <img src="https://magickiat.wordpress.com/wp-content/uploads/2017/01/spring-boot-logo.png" alt="springboot" width="40" height="40"/> </a>
  <a href="https://www.mongodb.com/" target="_blank" rel="noreferrer"> <img src="https://www.pngall.com/wp-content/uploads/13/Mongodb-PNG-Pic.png" alt="mongodb" width="60" height="40"/> </a>
  <a href="https://cucumber.io/docs/installation/java/" target="_blank" rel="noreferrer"> <img src="https://seeklogo.com/images/C/cucumber-logo-D727C551CE-seeklogo.com.png" alt="cucumeber" width="40" height="40"/> </a>
  <a href="https://kafka.apache.org/" target="_blank" rel="noreferrer"> <img src="https://static-00.iconduck.com/assets.00/kafka-icon-icon-1271x2048-pw1vvoye.png" alt="kafka" width="30" height="40"/> </a>
</p>